/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <maastie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/30 14:36:59 by maastie           #+#    #+#             */
/*   Updated: 2015/05/04 18:08:09 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_wlen(char const *s, char c, int i)
{
	int		k;
	char	l;

	k = 0;
	while ((s[i] && s[i] != '\0') && s[i] != c)
	{
		if (s[i] == '\"' || s[i] == '{')
		{
			l = s[i];
			if (s[i] == '{')
				l = '}';
			i++;
			k++;
			while (s[i] && (s[i] != '\0' && s[i] != l))
			{
				i++;
				k++;
			}
		}
		i++;
		k++;
	}
	return (k);
}

char		**ft_fill(char **ret, char const *s, char c, int r)
{
	int		i;

	i = 0;
	while (i < r)
	{
		ret[i] = NULL;
		i++;
	}
	i = 0;
	r = 0;
	while (s && (s[i] && s[i] != '\0'))
	{
		while ((s[i] && s[i] != '\0') && s[i] == c)
			i++;
		if (s[i] != '\0')
		{
			ret[r] = ft_strsub(s, i, ft_wlen(s, c, i));
			r++;
			i = ft_wlen(s, c, i) + i;
		}
	}
	return (ret);
}

char		**ft_strsplit(char const *s, char c)
{
	int		i;
	int		w;
	char	**ret;

	i = 0;
	w = 0;
	while (s && s[i])
	{
		while (s[i] && s[i] == c)
			i++;
		if (s[i] && (s[i] != c && s[i] != '\0'))
		{
			w++;
			while (s[i] && s[i] != c)
				i++;
		}
	}
	if ((ret = (char **)malloc(sizeof(char *) * (w + 1))) == NULL)
		return (NULL);
	if (w == 0)
		return (ret);
	return (ft_fill(ret, s, c, (w + 1)));
}
