/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 13:04:59 by dyuzan            #+#    #+#             */
/*   Updated: 2016/08/15 06:00:43 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

// #ifndef BUFF_SIZE
// # define BUFF_SIZE 32
// #elif BUFF_SIZE < 1 || BUFF_SIZE > 100000
// # undef BUFF_SIZE
// # define BUFF_SIZE 32
// #endif

int		go_to_line(char *tab, int *int_tab)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (i < *int_tab)
	{
		if (tab[j] == '\n')
			i++;
		j++;
		if (!tab[j])
			i++;
	}
	return (j);
}

char	*get_line(char *tab, char *line, int j)
{
	int			i;

	i = 0;
	while (tab[j] && tab[j] != '\n')
	{
		line[i] = tab[j];
		i++;
		j++;
	}
	line[i] = '\0';
	return (line);
}

char	*ft_join(char *s1, char *s2)
{
	int		i;
	int		j;
	char	*str;

	i = 0;
	j = 0;
	if (!(str = (char*)malloc(sizeof(*str) * (ft_strlen(s1) +
						ft_strlen(s2) + 1))))
		return (NULL);
	while (s1 && s1[i])
	{
		str[i] = s1[i];
		i++;
	}
	while (s2 && s2[j])
	{
		str[i] = s2[j];
		i++;
		j++;
	}
	if (s1)
		free(s1);
	str[i] = '\0';
	return (str);
}

int		check_next_line(int const fd, char **tab)
{
	int			ret;
	char		buf[BUFF_SIZE + 1];

	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		*tab = ft_join(*tab, buf);
		if (ft_strchr(buf, '\n'))
			return (1);
	}
	if (ret < 0)
		return (-1);
	if (!*tab)
		return (0);
	return (1);
}

int		get_next_line(int const fd, char **line)
{
	int					i;
	static t_buf		buf;

	if (fd < 0 || fd > 1024 || !line)
		return (-1);
	if (!buf.tab[fd])
		buf.int_tab[fd] = 0;
	i = check_next_line(fd, buf.tab + fd);
	if (i < 1)
		return (i);
	i = go_to_line(buf.tab[fd], buf.int_tab + fd);
	while (buf.tab[fd][i] && buf.tab[fd][i] != '\n')
		i++;
	if (!(*line = (char*)malloc(sizeof(char) * (i + 1))))
		return (-1);
	i = go_to_line(buf.tab[fd], buf.int_tab + fd);
	*line = get_line(buf.tab[fd], *line, i);
	buf.int_tab[fd]++;
	if (!buf.tab[fd][i])
	{
		ft_strdel(buf.tab + fd);
		return (0);
	}
	return (1);
}


// int	main (int argc, char **argv)
// {
// 	char *x;
// 	int  fd;

// 	fd = open(argv[1], O_RDONLY);
// 	if (fd >= 0)
// 	{
// 		while (get_next_line(fd, &x) > 0)
// 		{
// 			puts(x);
// 			//ft_putendl("============");
// 			free(x);
// 		}
// 	}
// 	else
// 		ft_putendl("OPEN FAILED");
// 	close(fd);
// 	return (0);
// }
