/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 13:06:36 by dyuzan            #+#    #+#             */
/*   Updated: 2016/06/29 13:07:19 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 32

#include <fcntl.h> // a supprimer
#include <stdio.h> // a delete 
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include "libft/libft.h"

typedef struct	s_buf
{
	char		*tab[1024];
	int			int_tab[1024];
}				t_buf;

int				go_to_line(char *tab, int *int_tab);
char			*get_line(char *tab, char *line, int j);
char			*ft_join(char *s1, char *s2);
int				check_next_line(int const fd, char **tab);
int				get_next_line(int const fd, char **line);

#endif
